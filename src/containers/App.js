import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";
import "dotenv";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import { PersistGate } from "redux-persist/integration/react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import PubNubReact from "pubnub-react";
import getMuiTheme from "material-ui/styles/getMuiTheme";

import { store } from "../store/configureStore";
import Routes from "./Routes";
import { persistor } from "../store/configureStore";

const pubnub = new PubNubReact({
  publishKey: "pub-c-c6e76a7e-54b3-41f9-a8de-a8e28ca7f2c4",
  subscribeKey: "sub-c-442f0abe-28a1-11e9-934b-52f171d577c7"
});

class App extends Component {
  componentDidMount = () => {};

  constructor(props) {
    super(props);
    pubnub.init(this);
  }

  UNSAFE_componentWillMount() {
    pubnub.subscribe({ channels: ["channel1"], withPresence: true });

    pubnub.getMessage("channel1", msg => {
      console.log(msg);
    });

    pubnub.getStatus(st => {
      console.log(st);
      pubnub.publish({ message: "hello world from react", channel: "channel1" });
    });
  }

  componentWillUnmount() {
    this.pubnub.unsubscribe({ channels: ["channel1"] });
  }

  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider muiTheme={getMuiTheme()}>
          <PersistGate loading={null} persistor={persistor}>
            <ToastContainer />
            <Routes />
          </PersistGate>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
