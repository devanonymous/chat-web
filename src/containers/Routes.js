import React from "react";
import { Route, Switch } from "react-router-dom";
import { Router } from "react-router-dom";
import { connect } from "react-redux";

import LoginForm from "../components/Auth/LoginForm";
import SignUpForm from "../components/Auth/SignUpForm";
import Main from "../components/Main";
import history from "../util/history";
import UpdatePassword from "../components/Auth/UpdatePassword";
import ForgotPassword from "../components/Auth/ForgotPassword";
import { SIGNUP, LOGIN, FORGOT_PASSWORD, UPDATE_PASSWORD, GETDOWN } from "../constants/routes";
import GetDown from "../components/Converse/GetDown";

class Routes extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path={LOGIN} component={LoginForm} />
          <Route path={SIGNUP} component={SignUpForm} />
          <Route path={FORGOT_PASSWORD} component={ForgotPassword} />
          <Route path={UPDATE_PASSWORD} component={UpdatePassword} />

          <Route path={GETDOWN} component={GetDown} />

          <Route path="/" component={!this.props.loginedUser ? LoginForm : Main} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return { loginedUser: state.auth.loginedUser };
};

export default connect(mapStateToProps)(Routes);
