import { toast } from "react-toastify";

import { SERVER_URL } from "../constants/serverUrl";
import {
  MESSAGES_LOADED_SUCCESS,
  MESSAGE_SENDED_SUCCESS,
  SEARCHING_MESSAGES
} from "../constants/actionTypes";

export const getMessages = dialogId => async dispatch => {
  const response = await fetch(SERVER_URL + "/messages/?dialogId=" + dialogId, {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json"
    })
  });
  if (response.ok) {
    dispatch({ type: MESSAGES_LOADED_SUCCESS, payload: await response.json() });
  } else {
    toast.error("Something wrong");
  }
};

export const sendMessage = (dialogId, text, isOperator = true) => async dispatch => {
  const response = await fetch(SERVER_URL + "/messages/", {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ dialogId, text, isOperator })
  });
  if (response.ok) {
    dispatch(getMessages(dialogId));
    dispatch({ type: MESSAGE_SENDED_SUCCESS });
  } else {
    toast.error("Sending error " + response.status);
  }
};

export const searchMessages = value => async dispatch => {
  if (value) {
    await dispatch({
      type: SEARCHING_MESSAGES,
      payload: { isSearching: true, searchingBy: value }
    });
  } else {
    await dispatch({ type: SEARCHING_MESSAGES, payload: { searchingBy: "", isSearching: false } });
  }
};
