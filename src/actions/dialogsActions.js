import { toast } from "react-toastify";

import { SERVER_URL } from "../constants/serverUrl";
import {
  DIALOGS_LOADING_SUCCESS,
  CHANGE_SECTION,
  AMOUNT_OF_NOT_ACTIVE_DIALOGS_LOADED_SUCCESS,
  DIALOGS_SEARCHING_SUCCESS
} from "../constants/actionTypes";
import { GET_DOWN_SECTION, SAVED_SECTION, CLOSED_SECTION, DIALOG_SECTION } from "../constants/sections";
import { sendMessage } from "./conversationAction";

export const getDialogs = (
  length = 0,
  section = GET_DOWN_SECTION,
  userId = null
) => async dispatch => {
  const response = await fetch(
    SERVER_URL + "/dialogs/?length=" + length + "&section=" + section + "&userId=" + userId,
    {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      })
    }
  );
  if (response.ok) {
    await dispatch({
      type: DIALOGS_LOADING_SUCCESS,
      payload: [await response.json(), section]
    });
  } else {
    await toast.error("Connection Error, Please try latter");
  }
};

export const moveToActiveHandle = (
  dialogId,
  userId,
  greeting = "Specialist is online"
) => async dispatch => {
  const response = await fetch(SERVER_URL + "/dialogs/dialog?id=" + dialogId, {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ userId })
  });
  if (response.ok) {
    toast.success("The dialog has been added to your active dialogs");
    await dispatch(sendMessage(dialogId, greeting, true));
    await dispatch(changeSection(GET_DOWN_SECTION));
  } else {
    toast.error("Somting's wrong");
  }
};

export const getAmountOfNotActiveDialogs = () => async dispatch => {
  const response = await fetch(SERVER_URL + "/dialogs/amount", {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json"
    })
  });
  if (response.ok) {
    dispatch({
      type: AMOUNT_OF_NOT_ACTIVE_DIALOGS_LOADED_SUCCESS,
      payload: await response.json()
    });
  }
};

export const changeSection = (section, dialog) => async dispatch => {
  dispatch({ type: CHANGE_SECTION, payload: { section, dialog } });
};

export const saveDialog = dialogId => async dispatch => {
  const response = await fetch(SERVER_URL + "/dialogs/save", {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ dialogId })
  });
  if (response.ok) {
    await dispatch(changeSection(CLOSED_SECTION));
  } else {
    toast.error("Somting's wrong");
  }
};

export const deleteDialog = dialogId => async dispatch => {
  const response = await fetch(SERVER_URL + "/dialogs/delete", {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ dialogId })
  });
  if (response.ok) {
    await dispatch(changeSection(SAVED_SECTION));
  } else {
    toast.error("Somting is wrong");
  }
};

export const startConversation = dialog => async dispatch => {
  await dispatch(changeSection(DIALOG_SECTION, dialog));
};

export const searchDialogs = (userId, searchingBy) => async dispatch => {
  if (searchingBy) {
    const response = await fetch(
      "/dialogs/search?userId=" + userId + "&searchingBy=" + searchingBy,
      {
        method: "GET",
        headers: new Headers({
          "Content-Type": "application/json"
        })
      }
    );
    if (response.ok) {
      const searchingResult = await response.json();
      await dispatch({
        type: DIALOGS_SEARCHING_SUCCESS,
        payload: { searchingResult: searchingResult.rows, isSearching: true }
      });
    } else {
      toast.error("Search error");
    }
  } else {
    await dispatch({
      type: DIALOGS_SEARCHING_SUCCESS,
      payload: { searchingResult: null, isSearching: false }
    });
    await dispatch(changeSection(GET_DOWN_SECTION));
  }
};
