import { toast } from "react-toastify";
import { SubmissionError } from "redux-form";

import history from "../util/history";
import { SERVER_URL } from "../constants/serverUrl";
import { LOGINING_HAS_ERRORED, LOGINING_SUCCESS, LOG_OUT } from "../constants/actionTypes";
import { LOGIN } from "../constants/routes";

/* TODO: проверить чтобы везде dispatch вызывался */

export function loginingHasErrored(payload) {
  return {
    type: LOGINING_HAS_ERRORED,
    payload: payload
  };
}

export function loginingSuccess(payload) {
  return {
    type: LOGINING_SUCCESS,
    payload: payload
  };
}

export const authWithEmail = data => async dispatch => {
  const response = await fetch(SERVER_URL + "/auth/sign_in", {
    method: "POST",
    body: JSON.stringify(data),
    headers: new Headers({
      "Content-Type": "application/json"
    })
  });
  if (response.ok) {
    const serverResponse = await response.json();
    dispatch(loginingSuccess(serverResponse));
    history.push("/");
  } else {
    throw new SubmissionError({ password: "Email or password is incorrect" });
  }
};

export const signUp = data => async dispatch => {
  const response = await fetch(SERVER_URL + "/auth/sign_up", {
    method: "POST",
    body: JSON.stringify(data),
    headers: new Headers({
      "Content-Type": "application/json"
    })
  });
  if (response.ok) {
    const serverResponse = await response.json();

    dispatch(loginingSuccess(serverResponse));
    history.push("/");
  } else if (response.status === 500) {
    throw new SubmissionError({ email: "This email is alredy exist" });
  }
};

export const resetPassword = email => async dispatch => {
  const response = await fetch(SERVER_URL + "/auth/reset_password?email=" + email, {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    })
  });
  if (response.ok) {
    toast.success("Success! Check out your email");
  } else {
    throw new SubmissionError({ email: "This email does not exist" });
  }
};

export const updatePassword = (password, token) => async dispatch => {
  const response = await fetch(SERVER_URL + "/auth/update_password", {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ password, token })
  });
  if (response.ok) {
    toast.success("The password has been updated");
    history.push(LOGIN);
  } else {
    throw new SubmissionError({ password: "Error" });
  }
};

export const validateToken = token => async dispatch => {
  const response = await fetch(SERVER_URL + "/auth/validate_token", {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ token })
  });
  if (!response.ok) {
    history.push(LOGIN);
  }
};

export const logOut = () => async dispatch => {
  history.push(LOGIN);
  dispatch({ type: LOG_OUT, payload: false });
};

// TODO: реализовать
export const authWithVk = vkResponse => async dispatch => {
  console.log("vk");
};
// TODO: реализовать
export const authWithGoogle = googleResponse => async dispatch => {
  console.log("google");
};
