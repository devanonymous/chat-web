import { toast } from "react-toastify";
import { PROFILE_UPDATED_SUCCESS } from "./../constants/actionTypes";
import { SERVER_URL } from "./../constants/serverUrl";
import { CONVERSATION_SETTING_UPDATED_SUCCESS } from "./../constants/actionTypes";

export const updateProfile = (data, userId) => async dispatch => {
  const response = await fetch("/setting/update_profile", {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ userId: userId, email: data.email, password: data.password })
  });
  if (response.ok) {
    toast.success("Profile has been updated successful");
    await dispatch({ type: PROFILE_UPDATED_SUCCESS });
  } else {
    toast.error("Somthing wrong");
  }
};

export const updateDialogsSetting = (data, userId) => async dispatch => {
  const response = await fetch(SERVER_URL + "/setting/apdate_dialogs_setting", {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json"
    }),
    body: JSON.stringify({ data, userId })
  });
  if (response.ok) {
    toast.success("Success");
    await dispatch({ type: CONVERSATION_SETTING_UPDATED_SUCCESS, payload: await response.json() });
  } else {
    toast.error("Error");
  }
};
