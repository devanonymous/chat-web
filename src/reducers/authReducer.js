import {
  LOGINING_HAS_ERRORED,
  LOGINING_SUCCESS,
  LOG_OUT,
  CONVERSATION_SETTING_UPDATED_SUCCESS
} from "../constants/actionTypes";

const initialState = {
  loginedUser: false
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGINING_HAS_ERRORED:
      return { ...state, loginedUser: action.payload };

    case LOGINING_SUCCESS:
      return { ...state, loginedUser: action.payload };

    case LOG_OUT:
      return { ...state, loginedUser: action.payload };
    case CONVERSATION_SETTING_UPDATED_SUCCESS:
      return { ...state, loginedUser: { ...state.loginedUser, user: action.payload.user[0] } };

    default:
      return state;
  }
};


