import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { authReducer } from "./authReducer";
import { dialogsReducer } from "./dialogsReducer";
import { conversationReducer } from "./conversationReducer";

export const rootReducer = combineReducers({
  form: formReducer,
  auth: authReducer,
  dialogs: dialogsReducer,
  conversation: conversationReducer
});
