import { MESSAGES_LOADED_SUCCESS, SEARCHING_MESSAGES } from "../constants/actionTypes";

const initialState = {
  searchingBy: "",
  isSearching: false,
  messages: []
};

export const conversationReducer = (state = initialState, action) => {
  switch (action.type) {
    case MESSAGES_LOADED_SUCCESS:
      return { ...state, messages: action.payload.messages };
    case SEARCHING_MESSAGES:
      return {
        ...state,
        searchingBy: action.payload.searchingBy,
        isSearching: action.payload.isSearching
      };

    default:
      return state;
  }
};
