import {
  DIALOGS_LOADING_SUCCESS,
  DOES_NOT_HAS_MORE_DIALOG,
  HAS_MORE_DIALOG,
  CHANGE_SECTION,
  ADDED_DIALOG_TO_ACTIVE,
  AMOUNT_OF_NOT_ACTIVE_DIALOGS_LOADED_SUCCESS,
  DIALOGS_SEARCHING_SUCCESS
} from "../constants/actionTypes";

import { GET_DOWN_SECTION } from "./../constants/sections";

const initialState = {
  hasMoreDialogs: false,
  isSearching: false,
  dialogs: [],
  section: GET_DOWN_SECTION,
  amountOfNotActiveDialogs: 0
};

export const dialogsReducer = (state = initialState, action) => {
  switch (action.type) {
    case DIALOGS_LOADING_SUCCESS:
      return {
        ...state,
        dialogs: action.payload[0].data.concat(state.dialogs),
        total: action.payload[0].total,
        section: action.payload[1]
      };
    case DOES_NOT_HAS_MORE_DIALOG:
      return { ...state, hasMoreDialogs: action.payload };
    case HAS_MORE_DIALOG:
      return { ...state, hasMoreDialogs: action.payload };
    case CHANGE_SECTION:
      return {
        ...state,
        section: action.payload.section,
        dialogForConversation: action.payload.dialog,
        dialogs: [],
        isSearching: false
      };
    case ADDED_DIALOG_TO_ACTIVE:
      return { ...state, addedDialogToActiveSuccess: action.payload };
    case AMOUNT_OF_NOT_ACTIVE_DIALOGS_LOADED_SUCCESS:
      return { ...state, amountOfNotActiveDialogs: action.payload.notActiveAmount };
    case DIALOGS_SEARCHING_SUCCESS:
      return {
        ...state,
        dialogs: action.payload.searchingResult,
        isSearching: action.payload.isSearching
      };
    default:
      return state;
  }
};
