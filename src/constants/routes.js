
// auth
export const SIGNUP = '/signup'
export const LOGIN = '/login'
export const FORGOT_PASSWORD = '/forgotpassword'
export const UPDATE_PASSWORD = '/update_password'

// Converse
export const ACTIVE = '/active'
export const COMPLETED = '/completed'
export const GETDOWN = '/getdown'
export const SAVED = '/saved'