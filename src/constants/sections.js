export const GET_DOWN_SECTION = "getDown";
export const ACTIVE_SECTION = "active";
export const SAVED_SECTION = "saved";
export const CLOSED_SECTION = "closed";
export const DIALOG_SECTION = "dialog";
