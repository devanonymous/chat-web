import React from "react";
import { Form, Button, ButtonGroup } from "reactstrap";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";

import "./form.css";
import FormField from "./FormField";
import { signUp } from "../../actions/authActions";
import { validate } from "./helpers/validation";
import AuthWithVk from "./AuthWithVK";
import AuthWithGoogle from "./AuthWithGoogle";
import { LOGIN } from "../../constants/routes";

class SignUpForm extends React.Component {
  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="wrapper">
        <Form
          className="wrapper__form"
          onSubmit={handleSubmit(values => this.props.signUp(values))}
        >
          <h2>Sign Up</h2>
          <Field name="email" type="email" component={FormField} label="Email" />
          <Field name="password" type="password" component={FormField} label="Password" />
          <Field
            name="confirm_password"
            type="password"
            component={FormField}
            label="Confirm the password"
          />
          <div>
            <div>
              <Link to={LOGIN}>log in</Link>
            </div>

            <Button className="submit_button" color="success" type="submit">
              Sign Up
            </Button>
          </div>
          <ButtonGroup className="social_buttons">
            <AuthWithVk />
            <AuthWithGoogle />
          </ButtonGroup>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signUp: data => dispatch(signUp(data))
  };
};

SignUpForm = reduxForm({
  form: "signUpForm",
  validate
})(SignUpForm);

export default connect(
  null,
  mapDispatchToProps
)(withRouter(SignUpForm));
