import React from "react";
import VkAuth from "react-vk-auth";
import { connect } from "react-redux";
import { authWithVk } from "../../actions/authActions";
require("dotenv").config();

const AuthWithVk = ({ authWithVk }) => (
  <VkAuth
    apiId={process.env.REACT_APP_VK_API_ID}
    className="btn btn-primary"
    callback={authWithVk}
    type="button"
  >
    VK
  </VkAuth>
);

export default connect(
  null,
  { authWithVk }
)(AuthWithVk);
