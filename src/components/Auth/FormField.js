import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

class FormField extends React.Component {
  render() {
    const { input, type, label, meta, data } = this.props;
    return (
      <FormGroup>
        <Label for="exampleEmail">{label}</Label>
        <Input
          className={
            meta.error && meta.touched
              ? "form-control is-invalid"
              : !input
              ? "form-control"
              : "form-control is-valid"
          }
          {...input}
          placeholder={label}
          type={type}
          value={data}
        />
        {meta.touched &&
          ((meta.error && <div className="invalid-feedback">{meta.error}</div>) ||
            (meta.warning && <div className="invalid-feedback">{meta.warning}</div>))}
      </FormGroup>
    );
  }
}

// <Input valid />

export default FormField;
