import React from "react";
import { Field, reduxForm } from "redux-form";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Form, Button } from "reactstrap";

import "./form.css";
import FormField from "./FormField";
import { LOGIN, SIGNUP } from "../../constants/routes";
import { validate } from "./helpers/validation";
import { resetPassword } from "../../actions/authActions";

class ForgotPassword extends React.Component {
  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="wrapper">
        <Form
          className="wrapper__form"
          onSubmit={handleSubmit(values => this.props.resetPassword(values.email))}
        >
          <h2>Forgot Password</h2>
          <Field name="email" type="email" component={FormField} label="Email" />
          <div className="links">
            <Link className="links__link" to={LOGIN}>
              log in
            </Link>
            <Link className="links__link" to={SIGNUP}>
              Sign up
            </Link>
          </div>
          <div>
            <Button className="submit_button" color="success" type="submit">
              Send
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: email => dispatch(resetPassword(email))
  };
};

ForgotPassword = reduxForm({
  form: "ForgotPassword",
  validate
})(ForgotPassword);

export default connect(
  null,
  mapDispatchToProps
)(withRouter(ForgotPassword));
