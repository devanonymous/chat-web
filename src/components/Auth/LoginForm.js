import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { Form, Label, Button, ButtonGroup } from "reactstrap";

import "./form.css";
import FormField from "./FormField";
import { authWithEmail } from "../../actions/authActions";
import { validate } from "./helpers/validation";
import AuthWithVk from "./AuthWithVK";
import AuthWithGoogle from "./AuthWithGoogle";
import { SIGNUP, FORGOT_PASSWORD } from "../../constants/routes";

class LoginForm extends React.Component {
  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="wrapper">
        <Form
          className="wrapper__form"
          onSubmit={handleSubmit(values => this.props.authWithEmail(values))}
        >
          <Label for="exampleEmail">Log in</Label>
          <Field name="email" type="email" component={FormField} label="Email" />
          <Field name="password" type="password" component={FormField} label="Password" />

          <div className="links">
            <Link className="links__link" to={SIGNUP}>
              Sign up
            </Link>

            <Link className="links__link" to={FORGOT_PASSWORD}>
              Forgot password
            </Link>
          </div>
          <div>
            <Button className="submit_button" color="success" type="submit">
              Login
            </Button>
          </div>
          <ButtonGroup className="social_buttons">
            <AuthWithVk />
            <AuthWithGoogle />
          </ButtonGroup>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    authWithEmail: data => dispatch(authWithEmail(data))
  };
};

LoginForm = reduxForm({
  form: "LoginForm",
  validate
})(LoginForm);

export default connect(
  null,
  mapDispatchToProps
)(withRouter(LoginForm));
