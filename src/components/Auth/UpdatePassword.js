import React from "react";
import { Field, reduxForm } from "redux-form";
import { Form, Button } from "reactstrap";
import FormField from "./FormField";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";

import { validate } from "./helpers/validation";
import { $_GET } from "./helpers/get";
import { LOGIN, SIGNUP } from "../../constants/routes";
import { updatePassword } from "../../actions/authActions";
class UpdatePassword extends React.Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="wrapper">
        <Form
          className="wrapper__form"
          onSubmit={handleSubmit(
            values =>
              this.props.updatePassword(values.password, $_GET("token")) /* TODO: react-router */
          )}
        >
          <Field name="password" type="password" component={FormField} label="Password" />
          <Field
            name="confirm_password"
            type="password"
            component={FormField}
            label="Confirm the password"
          />

          <div className="links">
            <Link className="links__link" to={LOGIN}>
              Log in
            </Link>
            <Link className="links__link" to={SIGNUP}>
              Sign up
            </Link>
          </div>
          <div>
            <Button className="submit_button" color="success" type="submit">
              Update password
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

UpdatePassword = reduxForm({
  form: "UpdatePassword",
  validate
})(UpdatePassword);

const mapDispatchToProps = dispatch => {
  return {
    updatePassword: (password, token) => dispatch(updatePassword(password, token))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withRouter(UpdatePassword));
