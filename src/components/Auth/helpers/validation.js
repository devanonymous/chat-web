export const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  if (!values.password) {
    errors.password = "Required";
  }
  if (values.confirm_password && values.password) {
    if (!values.password.match(/[0-9]/g, /[a-z]/g, /[A-Z]/g) && values.length >= 8) {
      errors.password =
        "the password must contain a number, lowercase and uppercase letters and must be at least 8 characters long";
    }
    if (values.confirm_password !== values.password) {
      errors.confirm_password = "The passwords are not equal";
    }
  }
  return errors;
};
