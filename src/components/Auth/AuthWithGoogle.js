import React from 'react'
import { GoogleLogin } from 'react-google-login'
import { connect } from 'react-redux'
import { authWithGoogle } from '../../actions/authActions'

const AuthWithGoogle = ({ authWithGoogle }) => (
  <GoogleLogin
    onSuccess={authWithGoogle}
    className="button"
    buttonText="Google"
    onFailure={console.error}
    clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
  />
)

export default connect(
  null,
  { authWithGoogle }
)(AuthWithGoogle)
