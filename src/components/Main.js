import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { validateToken } from "../actions/authActions";
import GetDown from "./Converse/GetDown";
class Main extends React.Component {
  componentDidMount = () => {
    // проверяем jwt токен
    this.props.validateToken(this.props.token);
  };

  render() {
    return (
      <>
        <GetDown />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.loginedUser.token
  };
};

const mapDispatchToProps = dispatch => {
  return {
    validateToken: token => dispatch(validateToken(token))
  };
};

Main.propTypes = {
  token: PropTypes.string.isRequired,
  validateToken: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
