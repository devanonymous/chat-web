import React from "react";
import PropTypes from "prop-types";
import InfiniteScroll from "react-infinite-scroller";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";

import AppLeftBar from "./AppLeftBar";
import Dialog from "./Dialog";
import Conversation from "./Messager/Conversation";
import { getDialogs } from "../../actions/dialogsActions";

import { DIALOG_SECTION } from "../../constants/sections";
import { getDownStyles } from "./styles";

/* TODO: исправить баг: при переключении в пустую секцию, назад уже не вернуться */
class GetDown extends React.Component {
  componentDidMount = async () => {
    // await this.props.getDialogs();
  };

  getDialogs = () => {
    const { section } = this.props;
    const { id: userId } = this.props.user.user;
    let length = 0;

    if (this.props.dialogs) {
      length = this.props.dialogs.length;
    }
    this.props.getDialogs(length, section, userId);
  };

  hasMore = () => {
    const { dialogs, total } = this.props;
    if (dialogs) {
      if (dialogs.length >= total) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  };

  forEachDialog = () =>
    this.props.dialogs &&
    this.props.dialogs.map((dialog, index) => <Dialog key={index} dialog={dialog} />);

  renderListOfDialogs = () => {
    return (
      <>
        <h2>Total: {this.props.total && this.props.total}</h2>
        <InfiniteScroll
          pageStart={0}
          loadMore={this.getDialogs}
          hasMore={this.hasMore()}
          loader={
            <div className="loader" key={0}>
              Loading ...
            </div>
          }
          useWindow={true}
        >
          {this.forEachDialog()}
        </InfiniteScroll>
      </>
    );
  };

  renderConversation = () => {
    return <Conversation />;
  };

  render() {
    const { classes, isSearching } = this.props;
    if (!isSearching) {
      return (
        <>
          <div className={classes.root}>
            <AppLeftBar />

            <main className={classes.content}>
              <div className={classes.toolbar} />

              {this.props.section === DIALOG_SECTION
                ? this.renderConversation()
                : this.renderListOfDialogs()}
            </main>
          </div>
        </>
      );
    }
    return (
      <>
        <div className={classes.root}>
          <AppLeftBar />

          <main className={classes.content}>
            <div className={classes.toolbar} />
            <h2>Finded: {this.props.dialogs.length}</h2>
            {this.forEachDialog()}
          </main>
        </div>
      </>
    );
  }
}

GetDown.propTypes = {
  classes: PropTypes.object.isRequired,
  dialogs: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
  isSearching: PropTypes.bool.isRequired,
  hasMoreDialogs: PropTypes.bool.isRequired,
  section: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  getDialogs: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    dialogs: state.dialogs.dialogs,
    total: Number(state.dialogs.total),
    hasMoreDialogs: state.dialogs.hasMoreDialogs,
    section: state.dialogs.section,
    user: state.auth.loginedUser,
    isSearching: state.dialogs.isSearching
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDialogs: (length, section, userId) => dispatch(getDialogs(length, section, userId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(getDownStyles)(withRouter(GetDown)));
