import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./App.css";
import { getMessages, sendMessage } from "../../../actions/conversationAction";
import Message from "./Message.js";
import Emoji from "./Emoji";
import FastResponse from "./FastResponse";
import Uploader from "./Uploader";

class Chatroom extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      chats: [
        {
          username: "Alice Chen",
          content: <p>Definitely! Sounds great!</p>,
          img: "http://i.imgur.com/Tj5DGiO.jpg"
        }
      ],
      inputValue: " ",
      seconds: 0,

      findedId: ""
    };

    this.submitMessage = this.submitMessage.bind(this);
    this.inputValueToState = this.inputValueToState.bind(this);
    this.pushEmoji = this.pushEmoji.bind(this);
    this.submitImage = this.submitImage.bind(this);
  }

  tick() {
    this.setState(prevState => ({
      seconds: prevState.seconds + 1
    }));
  }

  async componentDidMount() {
    await this.props.getMessages(this.props.dialogId);
    this.scrollToBot();
    this.interval = setInterval(() => this.tick(), 20000);
    this.scrollToFinded();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentDidUpdate() {
    this.scrollToBot();
    this.scrollToFinded();
    // this.scrollToFinded();
  }

  scrollToBot() {
    ReactDOM.findDOMNode(this.refs.chats).scrollTop = ReactDOM.findDOMNode(
      this.refs.chats
    ).scrollHeight;
  }

  scrollToFinded = () => {
    if (this.props.isSearching) {
      if (this.findedId) {
        const element = document.getElementById(this.findedId);
        if (element) {
          element.classList.add("found_message");
          element.scrollIntoView({
            behavior: "instant"
          });
        }
      }
    }
  };

  pushEmoji(emoji) {
    this.setState({ inputValue: this.state.inputValue + emoji });
  }

  submitMessage(e) {
    e.preventDefault();

    if (this.state.inputValue) {
      this.props.sendMessage(this.props.dialogId, this.state.inputValue);
    }
    this.setState({ inputValue: "" });
  }

  submitImage(info) {
    this.props.sendMessage(this.props.dialogId, info.originalUrl);
  }

  inputValueToState(event) {
    this.setState({ inputValue: event.target.value });
  }

  isFinded(message, whatToSearch) {
    whatToSearch = whatToSearch.toLowerCase();
    message.text = message.text.toLowerCase();

    if (message.text.indexOf(whatToSearch) >= 0) {
      this.findedId = "id" + message.id;
      return true;
    } else {
      return false;
    }
  }

  forEachMessages = () => {
    const { messages, clientName } = this.props;
    const { isSearching } = this.props;
    let array = [];
    if (!isSearching && messages) {
      array = messages.map(message => (
        <Message
          message={message}
          ref={null}
          clientName={clientName}
          id={"id" + message.id}
          key={message.id}
        />
      ));
    } else if (messages) {
      array = messages.map(message => {
        if (this.isFinded(message, this.props.searchingBy)) {
          return (
            <Message
              id={"id" + message.id}
              finded={true}
              className="finded"
              message={message}
              clientName={clientName}
              key={message.id}
            />
          );
        } else {
          return (
            <Message
              id={"id" + message.id}
              message={message}
              clientName={clientName}
              key={message.id}
            />
          );
        }
      });
    }
    return array;
  };

  test = item => {
    return item;
  };

  render() {
    return (
      <div className="chatroom">
        <h3>Conversation</h3>
        <ul className="chats" ref="chats">
          {this.forEachMessages().map(this.test)}
        </ul>

        <form className="input" onSubmit={this.submitMessage}>
          <input type="text" onChange={this.inputValueToState} value={this.state.inputValue} />

          <input type="submit" value="Submit" />
          <Emoji pushEmoji={this.pushEmoji} />
          <FastResponse pushEmoji={this.pushEmoji} />

          <Uploader
            id="file"
            name="file"
            onChange={file => {
              console.log("File changed: ", file);

              if (file) {
                file.progress(info => console.log("File progress: ", info.progress));
                file.done(info => console.log("File uploaded: ", info));
              }
            }}
            onUploadComplete={this.submitImage}
          />
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dialogId: state.dialogs.dialogForConversation.id,
    clientName: state.dialogs.dialogForConversation.client_name,
    messages: state.conversation.messages,
    isSearching: state.conversation.isSearching,
    searchingBy: state.conversation.searchingBy
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMessages: dialogId => dispatch(getMessages(dialogId)),
    sendMessage: (dialogId, text) => dispatch(sendMessage(dialogId, text))
  };
};

Chatroom.propTypes = {
  dialogId: PropTypes.number.isRequired,
  clientName: PropTypes.string.isRequired,
  messages: PropTypes.array.isRequired,
  isSearching: PropTypes.bool.isRequired,
  searchingBy: PropTypes.string.isRequired,
  getMessages: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chatroom);
