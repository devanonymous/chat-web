import React from "react";
import PropTypes from "prop-types";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Face from "@material-ui/icons/Face";
import { emoji } from "./helper/emoji";

const options = emoji;

const ITEM_HEIGHT = 48;

class Emoji extends React.Component {
  state = {
    anchorEl: null
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = option => event => {
    this.setState({ anchorEl: null });
    this.props.pushEmoji(option);
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div>
        <IconButton
          aria-label="More"
          aria-owns={open ? "long-menu" : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <Face />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200
            }
          }}
        >
          {options.map(option => (
            <MenuItem key={option} selected={option === "Pyxis"} onClick={this.handleClose(option)}>
              {option}
            </MenuItem>
          ))}
        </Menu>
      </div>
    );
  }
}
Emoji.propTypes = {
  pushEmoji: PropTypes.func.isRequired
};
export default Emoji;
