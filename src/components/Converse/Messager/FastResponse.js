import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import QuestionAnswer from "@material-ui/icons/QuestionAnswer";

const ITEM_HEIGHT = 48;

class FastResponse extends React.Component {
  state = {
    anchorEl: null
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
    // console.log(this.state.anchorEl);
  };

  handleClose = option => event => {
    this.setState({ anchorEl: null });
    // console.log(option);
    this.props.pushEmoji(option);
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div>
        <IconButton
          aria-label="More"
          aria-owns={open ? "long-menu" : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          <QuestionAnswer />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200
            }
          }}
        >
          {this.props.phrases.map(option => (
            <MenuItem key={option} selected={option === "Pyxis"} onClick={this.handleClose(option)}>
              {option}
            </MenuItem>
          ))}
        </Menu>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    phrases: JSON.parse(state.auth.loginedUser.user.phrases)
  };
};

FastResponse.propTypes = {
  pushEmoji: PropTypes.func.isRequired,
  phrases: PropTypes.array.isRequired
};

export default connect(mapStateToProps)(FastResponse);
