import React, { Component } from 'react';
import './App.css';

import Chatroom from './Chatroom.js';

class Conversation extends Component {
  render() {
    return (
      <div className="App">
        <Chatroom />
      </div>
    );
  }
}

export default Conversation;
