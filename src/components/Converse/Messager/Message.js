import React from "react";
import moment from "moment";
import PropTypes from "prop-types";

class Message extends React.Component {
  render() {
    const { message, clientName, finded } = this.props;

    return (
      <li className={`chat ${message.is_operator ? "right" : "left"} `} id={"id" + message.id}>
        {/* {user !== chat.username && <img src={chat.img} alt={`${chat.username}'s profile pic`} />} */}
        {<span className={finded ? "finded" : ""}>{message.text}</span>}
        {message.is_operator && <small>you</small> && (
          <small>{moment(message.created_at).fromNow()}</small>
        )}
        {!message.is_operator && <small>{clientName}</small>}
      </li>
    );
  }
}

Message.propTypes = {
  message: PropTypes.object.isRequired,
  clientName: PropTypes.string.isRequired,
  finded: PropTypes.bool
};

export default Message;
