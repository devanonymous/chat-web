import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import Save from "@material-ui/icons/Save";
import CheckCircle from "@material-ui/icons/CheckCircle";

import { appLeftBarStyles } from "./styles";
import PrimarySearchAppBar from "./AppTopBar";
import { changeSection } from "../../actions/dialogsActions";
import {
  GET_DOWN_SECTION,
  ACTIVE_SECTION,
  SAVED_SECTION,
  CLOSED_SECTION
} from "../../constants/sections";

export class AppLeftBar extends React.Component {
  getDownHandler = () => {
    this.props.changeSection(GET_DOWN_SECTION);
    // history.push(ACTIVE);
  };

  activeHandler = () => {
    this.props.changeSection(ACTIVE_SECTION);
  };

  completedHandler = () => {
    this.props.changeSection(CLOSED_SECTION);
  };

  savedHandler = () => {
    this.props.changeSection(SAVED_SECTION);
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <PrimarySearchAppBar />
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.toolbar} />
          <List>
            <ListItem button onClick={this.getDownHandler} key={1}>
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary={"Get down"} />
            </ListItem>

            <ListItem button onClick={this.activeHandler} key={2}>
              <ListItemIcon>
                <MailIcon />
              </ListItemIcon>
              <ListItemText primary={"Active"} />
            </ListItem>

            <ListItem onClick={this.completedHandler} button key={3}>
              <ListItemIcon>
                <CheckCircle />
              </ListItemIcon>
              <ListItemText primary={"Сompleted"} />
            </ListItem>

            <ListItem onClick={this.savedHandler} button key={4}>
              <ListItemIcon>
                <Save />
              </ListItemIcon>
              <ListItemText primary={"Saved"} />
            </ListItem>
          </List>
        </Drawer>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changeSection: section => dispatch(changeSection(section))
  };
};

AppLeftBar.propTypes = {
  classes: PropTypes.object.isRequired,
  changeSection: PropTypes.func.isRequired,
};

export default connect(
  null,
  mapDispatchToProps
)(withStyles(appLeftBarStyles)(AppLeftBar));
