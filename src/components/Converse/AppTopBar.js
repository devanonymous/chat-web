import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { withStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MailIcon from "@material-ui/icons/Mail";
import NotificationsIcon from "@material-ui/icons/Notifications";
import MoreIcon from "@material-ui/icons/MoreVert";

import { appTopBarStyles } from "./styles";

import { logOut } from "../../actions/authActions";
import { getAmountOfNotActiveDialogs } from "../../actions/dialogsActions";
import { searchDialogs } from "../../actions/dialogsActions";
import { searchMessages } from "../../actions/conversationAction";
import ProfileModal from "./Setting/ProfileModal";
import DialogsSettingModal from "./Setting/DialogsSettingModal";
import { DIALOG_SECTION } from "../../constants/sections";

class PrimarySearchAppBar extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null
  };

  componentDidMount = () => {
    this.props.getAmountOfNotActiveDialogs();
  };

  componentWillUpdate = () => {
    this.props.getAmountOfNotActiveDialogs();
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleLogOut = () => {
    this.props.logOut();
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  seatchDialogs = _.debounce(value => {
    this.props.searchDialogs(this.props.userId, value);
  }, 1500);

  seatchMessages = _.debounce(value => {
    console.log(value);
    this.props.searchMessages(value);
  }, 1500);

  onChange = e => {
    if (this.props.section !== DIALOG_SECTION) {
      this.seatchDialogs(e.target.value);
    } else {
      this.seatchMessages(e.target.value);
    }
  };

  render() {
    const { anchorEl } = this.state;
    const { classes } = this.props;
    const isMenuOpen = Boolean(anchorEl);

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        {" "}
        <ProfileModal email={this.props.email} userId={this.props.userId} />
        <DialogsSettingModal userId={this.props.userId} />
        <MenuItem onClick={this.handleLogOut}>Log out</MenuItem>
      </Menu>
    );

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography className={classes.title} variant="h6" color="inherit" noWrap>
              Chat-Web
            </Typography>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                onChange={this.onChange}
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
              />
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <IconButton color="inherit">
                <Badge badgeContent={0} color="secondary">
                  <MailIcon />
                </Badge>
              </IconButton>
              <IconButton color="inherit">
                <Badge
                  badgeContent={this.props.amountOfNotActiveDialogs}
                  max={999}
                  color="secondary"
                >
                  <NotificationsIcon />
                </Badge>
              </IconButton>
              <IconButton
                aria-owns={isMenuOpen ? "material-appbar" : undefined}
                aria-haspopup="true"
                onClick={this.handleProfileMenuOpen}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                <MoreIcon />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        {renderMenu}
        {/* {renderMobileMenu} */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    amountOfNotActiveDialogs: Number(state.dialogs.amountOfNotActiveDialogs),
    section: state.dialogs.section,
    userId: state.auth.loginedUser.user.id,
    email: state.auth.loginedUser.user.email
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logOut: () => dispatch(logOut()),
    getAmountOfNotActiveDialogs: () => dispatch(getAmountOfNotActiveDialogs()),
    searchDialogs: (userId, searchingBy) => dispatch(searchDialogs(userId, searchingBy)),
    searchMessages: value => dispatch(searchMessages(value))
  };
};

PrimarySearchAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
  amountOfNotActiveDialogs: PropTypes.number.isRequired,
  email: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
  section: PropTypes.string.isRequired,
  logOut: PropTypes.func.isRequired,
  getAmountOfNotActiveDialogs: PropTypes.func.isRequired,
  searchDialogs: PropTypes.func.isRequired,
  searchMessages: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(appTopBarStyles)(PrimarySearchAppBar));
