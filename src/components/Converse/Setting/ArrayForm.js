import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Field, FieldArray, reduxForm, arrayPush } from "redux-form";
import classNames from "classnames";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import { withStyles } from "@material-ui/core/styles";

import FormField from "../../Auth/FormField";
import { updateDialogsSetting } from "./../../../actions/settingActions";
import { phrases } from "./fields";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  iconSmall: {
    fontSize: 20
  }
});

class ArrayForm extends Component {
  UNSAFE_componentWillMount() {
    this.props.initialize({ phrases: this.props.phrases });
  }

  render() {
    const { handleSubmit, classes, userId } = this.props;

    return (
      <form
        className="facking_form"
        onSubmit={handleSubmit(data => this.props.updateDialogsSetting(data, userId))}
      >
        <FieldArray name="phrases" component={phrases} />
        <Field
          name="greeting"
          data={this.props.greeting}
          type="greeting"
          component={FormField}
          label="automatic greeting"
        />

        <Button type="submit" variant="contained" size="small" className={classes.button}>
          <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
          Save
        </Button>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    pushArray: arrayPush,
    updateDialogsSetting: (data, userId) => dispatch(updateDialogsSetting(data, userId))
  };
};

const mapStateToProps = state => {
  return {
    phrases: JSON.parse(state.auth.loginedUser.user.phrases),
    greeting: state.auth.loginedUser.user.greeting
  };
};

ArrayForm = reduxForm({
  form: "ArrayForm"
})(ArrayForm);

ArrayForm.propTypes = {
  classes: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
  phrases: PropTypes.array.isRequired,
  greeting: PropTypes.string.isRequired,
  updateDialogsSetting: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ArrayForm));
