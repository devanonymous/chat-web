import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Avatar from "@material-ui/core/Avatar";
import { withStyles } from "@material-ui/core/styles";

import FormField from "../../Auth/FormField";
import { validate } from "../../Auth/helpers/validation";
import { updateProfile } from "./../../../actions/settingActions";

import { profileModalStyles } from "./styles";

class ProfileModal extends React.Component {
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, handleSubmit, userId } = this.props;
    return (
      <>
        <MenuItem onClick={this.handleClickOpen}>Profile</MenuItem>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Profile</DialogTitle>

          <form onSubmit={handleSubmit(data => this.props.updateProfile(data, userId))}>
            <DialogContent>
              <Field
                name="email"
                data={this.props.email}
                type="email"
                component={FormField}
                label="Email"
              />

              <Avatar
                alt="Remy Sharp"
                src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg"
                className={classes.bigAvatar}
              />

              <input style={{ display: "inline-block" }} type="file" name="my_file" />

              <Field name="password" type="password" component={FormField} label="Password" />
              <Field
                name="confirm_password"
                type="password"
                component={FormField}
                label="Confirm the password"
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="secondary">
                Cancel
              </Button>
              <Button color="primary" type="submit" onClick={this.handleClose}>
                Update profile
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </>
    );
  }
}

ProfileModal = reduxForm({
  form: "ProfileModal",
  validate
})(ProfileModal);

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: (data, userId) => dispatch(updateProfile(data, userId))
  };
};

ProfileModal.propTypes = {
  classes: PropTypes.object.isRequired,
  email: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
  updateProfile: PropTypes.func.isRequired
};

export default connect(
  null,
  mapDispatchToProps
)(withStyles(profileModalStyles)(ProfileModal));
