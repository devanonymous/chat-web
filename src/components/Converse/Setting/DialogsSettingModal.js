import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import { reduxForm } from "redux-form";
import { withStyles } from "@material-ui/core/styles";

import { updateDialogsSetting } from "../../../actions/settingActions";
import ArrayForm from "./ArrayForm";

import { dialogsSettingModalStyles } from "./styles";

class DialogsSettingModal extends React.Component {
  state = {
    open: false
  };

  handleClick = open => () => this.setState({ open });

  render() {
    return (
      <>
        <MenuItem onClick={this.handleClick(true)}>Conversation setting</MenuItem>
        <Dialog
          open={this.state.open}
          onClose={this.handleClick(false)}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Conversation setting</DialogTitle>
          <ArrayForm userId={this.props.userId} />
        </Dialog>
      </>
    );
  }
}

DialogsSettingModal = reduxForm({
  form: "DialogsSettingModal"
})(DialogsSettingModal);

const mapDispatchToProps = dispatch => {
  return {
    updateDialogsSetting: (data, userId) => dispatch(updateDialogsSetting(data, userId))
  };
};

DialogsSettingModal.propTypes = {
  classes: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
  updateDialogsSetting: PropTypes.func.isRequired
};

export default connect(
  null,
  mapDispatchToProps
)(withStyles(dialogsSettingModalStyles)(DialogsSettingModal));
