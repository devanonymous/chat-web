import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import moment from "moment";

import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import MoveToInbox from "@material-ui/icons/MoveToInbox";
import Textsms from "@material-ui/icons/Textsms";
import Save from "@material-ui/icons/Save";

import Delete from "@material-ui/icons/Delete";

import { randomInteger } from "./helper/random";
import {
  moveToActiveHandle,
  saveDialog,
  deleteDialog,
  startConversation
} from "./../../actions/dialogsActions";
import {
  GET_DOWN_SECTION,
  ACTIVE_SECTION,
  SAVED_SECTION,
  CLOSED_SECTION
} from "../../constants/sections";
import { dialogStyles } from "./styles";

class Dialog extends React.Component {
  moveToActiveHandle = () => {
    const userId = this.props.user.user.id;
    const dialogId = this.props.dialog.id;
    const greeting = this.props.greeting ? this.props.greeting : "specialist is online";
    this.props.moveToActiveHandle(dialogId, userId, greeting);
  };

  saveDialogHandle = () => {
    const dialogId = this.props.dialog.id;
    this.props.saveDialog(dialogId);
  };

  deleteDialog = () => {
    const dialogId = this.props.dialog.id;
    this.props.deleteDialog(dialogId);
  };

  startConversationHandle = () => {
    const dialog = this.props.dialog;
    this.props.startConversation(dialog);
  };

  renderButton() {
    const is_saved = this.props.dialog.is_saved;
    const { classes } = this.props;
    if (!this.props.isSearching) {
      if (this.props.section === GET_DOWN_SECTION) {
        return (
          <Button
            variant="contained"
            color="default"
            className={classes.button}
            onClick={this.moveToActiveHandle}
          >
            Move to active
            <MoveToInbox className={classes.rightIcon} />
          </Button>
        );
      } else if (this.props.section === ACTIVE_SECTION) {
        return (
          <Button
            variant="contained"
            color="default"
            className={classes.button}
            onClick={this.startConversationHandle}
          >
            Start conversation
            <Textsms className={classes.rightIcon} />
          </Button>
        );
      } else if (this.props.section === CLOSED_SECTION) {
        return (
          <Button
            variant="contained"
            color="default"
            disabled={is_saved ? true : false}
            className={classes.button}
            onClick={this.saveDialogHandle}
          >
            Save dialog
            <Save className={classes.rightIcon} />
          </Button>
        );
      } else if (this.props.section === SAVED_SECTION) {
        return (
          <Button
            variant="contained"
            color="default"
            className={classes.button}
            onClick={this.deleteDialog}
          >
            Delete
            <Delete className={classes.rightIcon} />
          </Button>
        );
      }
    } else {
      return (
        <Button
          variant="contained"
          color="default"
          className={classes.button}
          onClick={this.startConversationHandle}
        >
          Start conversation
          <Textsms className={classes.rightIcon} />
        </Button>
      );
    }
  }

  render() {
    const { classes } = this.props;
    const { client_name: clientName } = this.props.dialog;
    const { id } = this.props.dialog;
    return (
      <List>
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar
              className={randomInteger(1, 2) === 1 ? classes.purpleAvatar : classes.orangeAvatar}
            >
              {clientName[0]}
            </Avatar>
          </ListItemAvatar>

          <ListItemText
            primary="" /* TODO: Сюда запихивать тему обращения */
            secondary={
              <React.Fragment>
                <small style={{ display: "block", fontStyle: "italic" }}>
                  {moment(this.props.dialog.created_at).fromNow()}
                </small>
                <Typography component="span" className={classes.inline} color="textPrimary">
                  {clientName} id: {id} {/* TODO: Сюда пихать имя клиента */}
                </Typography>
                {this.props.dialog.last_message} {/* TODO: Текст запроса */}
              </React.Fragment>
            }
          />
          {this.renderButton()}
        </ListItem>
      </List>
    );
  }
}

Dialog.propTypes = {
  classes: PropTypes.object.isRequired,
  dialog: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  section: PropTypes.string.isRequired,
  isSearching: PropTypes.bool.isRequired,
  greeting: PropTypes.string.isRequired,
  moveToActiveHandle: PropTypes.func.isRequired,
  saveDialog: PropTypes.func.isRequired,
  deleteDialog: PropTypes.func.isRequired,
  startConversation: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth, dialogs }) => {
  return {
    user: auth.loginedUser,
    section: dialogs.section,
    isSearching: dialogs.isSearching,
    greeting: auth.loginedUser.user.greeting
  };
};

const mapDispatchToProps = dispatch => {
  return {
    moveToActiveHandle: (dialogId, userId, greeting) =>
      dispatch(moveToActiveHandle(dialogId, userId, greeting)),
    saveDialog: dialogId => dispatch(saveDialog(dialogId)),
    deleteDialog: dialogId => dispatch(deleteDialog(dialogId)),
    startConversation: dialogId => dispatch(startConversation(dialogId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(dialogStyles)(Dialog));
